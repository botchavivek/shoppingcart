name := "ShoppingCart"

scalaVersion := "2.11.1"


libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.1.7" % "test",
  "org.mockito" % "mockito-core" % "1.9.5"
)