package org.hmrc.shoppingcart

import org.hmrc.shoppingcart.model.{ItemType, Orange, Apple}

object CheckOut {
  def totalOf(shoppingCart :ShoppingCart, offers: (ShoppingCart => BigDecimal)*): String = {
    val totalPrice: BigDecimal = shoppingCart.getItems.foldLeft(BigDecimal(0.0))((sum, item) => sum + item.itemType.price * item.quantity)
    val discountPrice: BigDecimal = offers.map(offer => offer(shoppingCart)).sum
    println("£%.2f".format(totalPrice - discountPrice))

    "£%.2f".format(totalPrice - discountPrice)
  }
}

object Offers {
  def applesDiscount(shoppingCart: ShoppingCart): BigDecimal = (getTotalItemsByItemType(shoppingCart, Apple) / 2) * Apple.price
  def orangesDiscount(shoppingCart: ShoppingCart): BigDecimal = (getTotalItemsByItemType(shoppingCart, Orange) / 3) * Orange.price

  private def getTotalItemsByItemType(shoppingCart: ShoppingCart, itemType: ItemType): Int = {
    shoppingCart.getItems
      .filter(_.itemType == itemType)
      .foldLeft(0)((totalApples, item) => totalApples + item.quantity)
  }

}