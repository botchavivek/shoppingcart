package org.hmrc.shoppingcart

import org.hmrc.shoppingcart.exceptions.ItemNotFound
import org.hmrc.shoppingcart.model.{ItemType, Orange, Apple, Item}

object ShoppingCart {
  def apply() = new ShoppingCart()
}

class ShoppingCart() {
  private var items: Seq[Item] = Seq[Item]()

  def addItem(item: Item) = item.itemType match {
    case Apple | Orange => items = items :+ item; this
    case _ => throw new ItemNotFound("we only sell apples and oranges.")

  }

  def getItems: Seq[Item] = items
}

