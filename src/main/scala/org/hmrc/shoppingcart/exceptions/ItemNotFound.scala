package org.hmrc.shoppingcart.exceptions

class ItemNotFound(message: String) extends RuntimeException(message)