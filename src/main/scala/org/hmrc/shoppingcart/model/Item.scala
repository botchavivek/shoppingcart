package org.hmrc.shoppingcart.model

case class Item(itemType: ItemType, quantity: Int)
