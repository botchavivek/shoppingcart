package org.hmrc.shoppingcart.model

object Apple extends ItemType("Apple", 0.60)
object Orange extends ItemType("Orange", 0.25)
case class ItemType(name: String, price: BigDecimal)

