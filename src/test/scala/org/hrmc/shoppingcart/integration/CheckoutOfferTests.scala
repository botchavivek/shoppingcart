package org.hrmc.shoppingcart.integration


import org.hmrc.shoppingcart.ShoppingCart
import org.hmrc.shoppingcart.model.{Apple, Item, Orange}
import org.scalatest.FunSpec
import org.scalatest.Matchers._

class CheckoutOfferTests extends FunSpec {

  import org.hmrc.shoppingcart.CheckOut._
  import org.hmrc.shoppingcart.Offers._

  it("get total purchase amount when buy one, get one free on Apples offer enabled") {

    //given //when //then
    totalOf(ShoppingCart().addItem(Item(Apple, 1)), applesDiscount) shouldBe "£0.60"
    totalOf(ShoppingCart().addItem(Item(Apple, 2)), applesDiscount) shouldBe "£0.60"
    totalOf(ShoppingCart().addItem(Item(Apple, 3)), applesDiscount) shouldBe "£1.20"
    totalOf(ShoppingCart().addItem(Item(Apple, 4)), applesDiscount) shouldBe "£1.20"
  }

  it("get total purchase amount when 3 for the price of 2 on Oranges offer enabled") {
    //given //when //then
    totalOf(ShoppingCart().addItem(Item(Orange, 2)), orangesDiscount) shouldBe "£0.50"
    totalOf(ShoppingCart().addItem(Item(Orange, 3)), orangesDiscount) shouldBe "£0.50"
    totalOf(ShoppingCart().addItem(Item(Orange, 4)), orangesDiscount) shouldBe "£0.75"
    totalOf(ShoppingCart().addItem(Item(Orange, 5)), orangesDiscount) shouldBe "£1.00"
    totalOf(ShoppingCart().addItem(Item(Orange, 6)), orangesDiscount) shouldBe "£1.00"
  }

  it("get total purchase amount when offers on oranges and apples") {
    //given //when //then
    totalOf(ShoppingCart().addItem(Item(Apple, 2)).addItem(Item(Orange, 3)), applesDiscount, orangesDiscount) shouldBe "£1.10"

  }
}
