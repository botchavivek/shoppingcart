package org.hrmc.shoppingcart.integration

import org.hmrc.shoppingcart.ShoppingCart
import org.hmrc.shoppingcart.model.{Apple, Item, Orange}
import org.scalatest.FunSpec
import org.scalatest.Matchers._

class CheckoutTests extends FunSpec{

  import org.hmrc.shoppingcart.CheckOut._

  it("purchase amount for 1 apple") {
    //given
    val cart: ShoppingCart = ShoppingCart().addItem(Item(Apple, 1))
    //when & then
    totalOf(cart) shouldBe "£0.60"
  }

  it("purchase amount for 1 orange") {
    //given
    val cart: ShoppingCart = ShoppingCart().addItem(Item(Orange, 1))
    //when & then
    totalOf(cart) shouldBe "£0.25"
  }

  it("purchase amount for 2 apples") {
    //given
    val cart: ShoppingCart = ShoppingCart().addItem(Item(Apple, 2))
    //when & then
    totalOf(cart) shouldBe "£1.20"
  }

  it("purchase amount for 2 oranges") {
    //given
    val cart: ShoppingCart = ShoppingCart().addItem(Item(Orange, 2))
    //when & then
    totalOf(cart) shouldBe "£0.50"
  }

  it("purchase amount for 3 apples and one orange") {
    //given
    val cart: ShoppingCart = ShoppingCart().addItem(Item(Apple, 2)).addItem(Item(Orange, 1)).addItem(Item(Apple, 1))
    //when & then
    totalOf(cart) shouldBe "£2.05"
  }
}
