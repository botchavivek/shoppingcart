package org.hrmc.shoppingcart.integration

import org.hmrc.shoppingcart._
import org.hmrc.shoppingcart.model._
import org.hmrc.shoppingcart.exceptions.ItemNotFound
import org.scalatest.FunSpec
import org.scalatest.Matchers._
class ShoppingCartTests extends FunSpec {


  it("add apples") {
    //given
    val cart = new ShoppingCart()
    val apples = Seq(Item(Apple, 1), Item(Apple, 2))
    //when
    apples.foreach(cart.addItem)
    //then
    cart.getItems shouldBe apples
  }

  it("add oranges") {
    //given
    val cart = new ShoppingCart()
    val oranges = Seq(Item(Orange, 1), Item(Orange, 2))

    //when
    oranges.foreach(cart.addItem)
    //then
    cart.getItems shouldBe oranges
  }

  it("add apples and oranges") {
    //given
    val cart = new ShoppingCart()
    val applesAndOranges = Seq(Item(Orange, 1), Item(Orange, 2))

    //when
    applesAndOranges.foreach(cart.addItem)
    //then
    cart.getItems shouldBe applesAndOranges
  }

  it("not allow to add other than apples and orange") {
    //given
    val cart = new ShoppingCart()
    //when
    intercept[ItemNotFound] {
      cart.addItem(Item(ItemType("Mango", 0.50), 1))
    }
  }

}
